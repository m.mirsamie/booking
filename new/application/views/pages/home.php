<?php
$_SESSION['refrence_id'] = NULL;
$_SESSION['voucher_id'] = NULL;
$_SESSION['data'] = NULL;
$_SESSION['adl'] = NULL;
$_SESSION['chd'] = NULL;
$_SESSION['inf'] = NULL;
$_SESSION['data2'] = NULL;
$_SESSION['price'] = NULL;
$_SESSION['passengers'] = NULL;
$_SESSION['start_time'] = NULL;
$_SESSION['time'] = NULL;
$_SESSION['state'] = NULL;
$err = '';
if (isset($_REQUEST['err']) && trim($_REQUEST['err'])) {
    $e = trim($_REQUEST['err']);
    $err = <<<ERR
            alert('$e');
ERR;
}
$my = new mysql_class ();
$query = "SELECT * FROM ads_img WHERE is_home=1 ORDER BY tartib";
$my->ex_sql($query, $outs);
$adsRes = "";
foreach ($outs as $out) {
    $url = $out ['url'];
    $tmp = "<img class='img-responsive' src=\"%url%\">";
    $adsRes .= str_replace("%url%", $url, $tmp);
}

$tour_tmp = <<<TTMP
                <div class="col-sm-6" style="#padding# margin-bottom:5px;">
                    <div style="border: 1px solid #34363d; border-radius: 5px;">
                        <img src="#url#" width="100%" > 
                        <p style="padding: 5px 10px;">#tourtitle#<span style="float:left; color: #33a0d7; cursor: pointer;" onclick="toggleDet(this);">جزئیات بیشتر</span></p>
                    </div>
                    <div style="#padding# border: 1px solid #34363d; border-radius: 5px;position:absolute; top:0px; width:96%; height:100%;background:white;display:none;">
                        <div style="padding: 10px;">
                            #tourcontent#
                        </div>
                        <span style="position:absolute; bottom:15px; left:15px; cursor: pointer; color: #33a0d7;" onclick="$(this).parent().slideUp();">خروج</spana>
                    </div>
                </div>
TTMP;
$querytour = "SELECT * FROM tour_img ORDER BY tartib";
$my->ex_sql($querytour, $outstour);
$tourRes = "";
foreach ($outstour as $i => $outtour) {
    $tmp = str_replace("#url#", $outtour['url'], $tour_tmp);
    $tmp = str_replace("#tourtitle#", $outtour['tourtitle'], $tmp);
    $tmp = str_replace("#tourcontent#", $outtour['tourcontent'], $tmp);
    $tmp = str_replace("#padding#", (($i % 2 == 0) ? 'padding-right: 0;' : 'padding-left: 0;'), $tmp);
    $tourRes .= $tmp;
}

$dat = 0;
if (isset($_REQUEST['dat'])) {
    $dat = (int) $_REQUEST['dat'];
}
$results = array();

$result_fare = search_class::loadLowFare($dat);
if (isset($result_fare["data"])) {
    $results = $result_fare["data"];
    foreach ($results as $i => $res) {
        $results[$i]['from_city_small'] = city_class::loadByIata($res['from_city']); //$this->inc_model->substrH(city_class::loadByIata($res['from_city']), 5);
        $results[$i]['to_city_small'] = city_class::loadByIata($res['to_city']); //$this->inc_model->substrH(city_class::loadByIata($res['to_city']), 5);
        $results[$i]['from_city_name'] = city_class::loadByIata($res['from_city']);
        $results[$i]['to_city_name'] = city_class::loadByIata($res['to_city']);
        $results[$i]['price_monize'] = $this->inc_model->monize($res['price'] / 10);
    }
}
for ($i = 0; $i < 8; $i++) {
    if (!isset($results[$i])) {
        $results[$i]['from_city_small'] = '----';
        $results[$i]['to_city_small'] = '----';
        $results[$i]['from_city_name'] = '----';
        $results[$i]['to_city_name'] = '----';
        $results[$i]['price_monize'] = '----';
    }
}
$sign = ($dat < 0) ? ' - ' : ' + ';
$dat = abs($dat);
$result_fare['tarikh'] = jdate("Y/m/d", strtotime(date("Y-m-d") . $sign . $dat . ' day'));
$result_fare["data"] = $results;
if (isset($_REQUEST['dat'])) {
    die(json_encode($result_fare));
}
?>
<div class="container-fluid">
    <div class="row" style="background-image: url('<?php echo asset_url() ?>images/img/mainhead.PNG'); background-repeat: no-repeat; background-size: 100% auto; background-color: #aad5ea; border-bottom: 2px solid #fff;">
        <div class="col-lg-2 col-md-1"></div>
        <div class="col-lg-8 col-md-10 search-box" style="margin-top: 16%;">
            <header>
                جستجوی پرواز
            </header>
            <form class="form-inline hidden-xs" method="post" action="search">
                <span>یک طرفه<input name="way" type="radio" value="one" checked></span>
                <span>دو طرفه<input name="way" type="radio" disabled></span>
                <br>
                <select class="sel1" name="from_city">
                    <option value="">
                        <?php echo city_class::loadAll(); ?>
                    </option>
                </select>
                <select class="sel2" name="to_city">
                    <option value="">
                        <?php echo city_class::loadAll(); ?>
                    </option>
                </select>
                <input type="text" name="aztarikh" class="form-control dateValue2 aztarikh">
                <input type="text" name="tatarikh" class="form-control dateValue2 tatarikh">
                <br>
                <button type="submit">جستجو</button>
            </form>

            <form class="form-inline visible-xs" method="post" action="search">
                <span>یک طرفه<input type="radio" name="way" value="one" checked></span>
                <span>دو طرفه<input type="radio" name="way" disabled></span>
                <br>
                <select style="width: 100% !important;" class="sel1" name="from_city">
                    <option value="" disabled selected>مبدا</option>
                    <option value="">
                        <?php echo city_class::loadAll(); ?>
                    </option>
                </select>
                <select style="width: 100% !important;" class="sel2" name="to_city">
                    <option value="" disabled selected>مقصد</option>
                    <option value="">
                        <?php echo city_class::loadAll(); ?>
                    </option>
                </select>
                <input style="width: 100% !important;" type="text" name="aztarikh" class="form-control dateValue2">
                <input style="width: 100% !important;" type="text" name="tatarikh" class="form-control dateValue2">
                <br>
                <button type="submit">جستجو</button>
            </form>
        </div>
        <div class="col-lg-2 col-md-1"></div>
    </div>
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-sm-12 web-info">
            این وب سایت جهت خرید پرواز به صورت یک طرفه می باشد. لطفا برای خرید پرواز دوطرفه، دو مسیر
            <br>
            رفت و برگشت را جداگانه جستجو کرده و پروازهای مورد نظر را به صورت مستقل خریداری نمایید.
        </div>
    </div>
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-lg-2 col-md-1"></div>
        <div class="col-lg-8 col-md-10 cell">
            <span style="border-bottom: 1px dotted #6396b3; padding-bottom: 5px;">ارزان ترین نرخ در<span class="tat"><?php echo jdate("Y/m/d"); ?></span></span>
            <div style="margin-top: 20px;">
                <table>
                    <tr>
                        <td rowspan="2">
                            <a href="javascript:backFare();"><span class="glyphicon glyphicon-chevron-right"></span></a>
                        </td>
                        <td>
                            <div id="fl_large_0">
                                <header><?php echo $results[0]['from_city_name']; ?> - <?php echo $results[0]['to_city_name'] ?></header>
                                <span><?php echo $results[0]['price_monize']; ?> تومان</span>
                            </div>
                        </td>
                        <td class="hidden-xs">
                            <div id="fl_large_1">
                                <header><?php echo $results[1]['from_city_name']; ?> - <?php echo $results[1]['to_city_name'] ?></header>
                                <span><?php echo $results[1]['price_monize']; ?> تومان</span>
                            </div>
                        </td>
                        <td class="hidden-xs">
                            <div id="fl_large_2">
                                <header><?php echo $results[2]['from_city_name']; ?> - <?php echo $results[2]['to_city_name'] ?></header>
                                <span><?php echo $results[2]['price_monize']; ?> تومان</span>
                            </div>
                        </td>
                        <td class="hidden-xs hidden-sm">
                            <div id="fl_large_3">
                                <header><?php echo $results[3]['from_city_name']; ?> - <?php echo $results[3]['to_city_name'] ?></header>
                                <span><?php echo $results[3]['price_monize']; ?> تومان</span>
                            </div>
                        </td>
                        <td rowspan="2">
                            <a href="javascript:nextFare();"><span class="glyphicon glyphicon-chevron-left"></span></a>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div id="fl_large_4">
                                <header><?php echo $results[4]['from_city_name']; ?> - <?php echo $results[4]['to_city_name'] ?></header>
                                <span><?php echo $results[4]['price_monize']; ?> تومان</span>
                            </div>
                        </td>
                        <td class="hidden-xs">
                            <div id="fl_large_5">
                                <header><?php echo $results[5]['from_city_name']; ?> - <?php echo $results[5]['to_city_name'] ?></header>
                                <span><?php echo $results[5]['price_monize']; ?> تومان</span>
                            </div>
                        </td>
                        <td class="hidden-xs">
                            <div id="fl_large_6">
                                <header><?php echo $results[6]['from_city_name']; ?> - <?php echo $results[6]['to_city_name'] ?></header>
                                <span><?php echo $results[5]['price_monize']; ?> تومان</span>
                            </div>
                        </td>
                        <td class="hidden-xs hidden-sm">
                            <div id="fl_large_7">
                                <header><?php echo $results[7]['from_city_name']; ?> - <?php echo $results[7]['to_city_name'] ?></header>
                                <span><?php echo $results[5]['price_monize']; ?> تومان</span>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-lg-2 col-md-1"></div>
    </div>
    <div class="row" style="background-color: #f2f3f5;">
        <div class="col-lg-2 col-md-1"></div>
        <div class="col-lg-4 col-md-5 address">
            <div>
                <header>تماس با ما</header>
                <img class="img-responsive" src="<?php echo asset_url() ?>images/img/ansar-address.PNG">
                <p>Tell : (+9821) &nbsp &nbsp 44 50 12 34</p>
                <p>Reservation : 093505255746</p>
                <p>Fax : (+9851) &nbsp &nbsp 89 78 20 83</p>
            </div>
        </div>
        <div class="col-lg-4 col-md-5 contact">
            <form>
                <header>ارتباط با ما</header>
                <input type="text" placeholder="نام و نام خانوادگی">
                <input type="text" placeholder="پست الکترونیک">
                <input type="text" placeholder="شماره همراه">
                <textarea placeholder="متن پیام"></textarea>
                <button type="submit">ارسال</button>
            </form>
            <div class="col-sm-6"></div>
        </div>
        <div class="col-lg-2 col-md-1"></div>
    </div>
</div>

<script>
    function toggleDet(dobj)
    {
        var obj = $(dobj).parent().parent().next();
        obj.slideDown();
    }
<?php echo $err; ?>
</script>