<!DOCTYPE html>
<html>
    <head>
        <?php
        $this->load->helper('html');
        $this->load->helper('url');
        echo meta($meta);
        ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            <?php echo $title; ?>
        </title>
        <link rel="icon" href="<?php echo asset_url(); ?>images/img/fav.png" type="image/png" sizes="16x16">
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap-rtl.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/crm.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/stylesheet.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/font-awesome.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap-select.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/select2.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/xgrid.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/fileinput.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap-datepicker.min.css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/persian-datepicker.css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap-select.min.css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/reserve.css" />
        <script src=" <?php echo asset_url() . 'js/jquery-1.11.1.min.js' ?>" ></script>
        <script src=" <?php echo asset_url() . 'js/bootstrap.min.js' ?>" ></script>
        <script src=" <?php echo asset_url() . 'js/grid.js' ?>" ></script>
        <script src=" <?php echo asset_url() . 'js/bootstrap-datepicker.min.js' ?>"></script>
        <script src=" <?php echo asset_url() . 'js/bootstrap-datepicker.fa.min.js' ?>"></script>
        <script src=" <?php echo asset_url() . 'js/fileinput.min.js' ?>"></script>
        <script src=" <?php echo asset_url() . 'js/select2.min.js' ?>"></script>
        <script src=" <?php echo asset_url() . 'js/jquery.PrintArea.js' ?>"></script>
        <script src=" <?php echo asset_url() . 'js/persian-date.js' ?>" ></script>
        <script src=" <?php echo asset_url() . 'js/persian-datepicker.min.js' ?>" ></script>
        <script src=" <?php echo asset_url() . 'bootstrap-select.min' ?>" ></script>
        <?php
        if (isset($has_ckeditor) && $has_ckeditor) { // if in edit_content ckedotr is activeted
            echo '<script src="' . asset_url() . 'js/ckeditor/ckeditor.js"></script>' . "\n";
        }
        ?>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row" style="border-bottom: 2px solid #fff;">
                <div class="col-md-12 main-header">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-4">
                            <p class="hidden-sm hidden-xs">
                                <?php echo jdate("Y/m/d"); ?>
                                |
                                <?php echo date("Y/m/d"); ?>
                            </p>
                        </div>
                        <div class="col-md-4">
                            <img class="visible-lg visible-md pull-left" src="<?php echo asset_url() ?>images/img/logo-large.png">
                            <img class="visible-sm visible-xs pull-left" src="<?php echo asset_url() ?>images/img/logo-small.png">
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </div>