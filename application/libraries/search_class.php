<?php

require_once "lib/nusoap.php";

//class SoapHeaderUsernameToken {
//
//    public $Password;
//    public $Username;
//
//    public function __construct($l, $p) {
//        $this->Password = $p;
//        $this->Username = $l;
//    }
//
//}

class search_class {

    public static function sendRequest($url, $user, $pass, $data) {
        $options = array(
            'http' => array(
                'header' => "User: $user\r\nPassword: $pass\r\nContent-Type: application/json\r\n",
                'method' => 'POST',
                'content' => $data
            ),
        );
        $context = stream_context_create($options);
        $out = file_get_contents($url, false, $context);
        return($out);
    }

    public static function loadLowFare($dat, $count = 8) {
        $conf = new conf();
        $url = $conf->wurl . "rest/loadLowFare";
        $User = $conf->wuser;
        $Password = $conf->wpass;
        $req = "[
                    $dat,
                    $count
                ]";
        $content = search_class::sendRequest($url, $User, $Password, $req);
        $out = json_decode($content, TRUE);
        return($out);
    }

    public static function search($date, $rdate, $from_city, $to_city, $extra, $airlines, $sort, $way) {

        function perToEnNums($inNum) {
            $outp = $inNum;
            $outp = str_replace('۰', '0', $outp);
            $outp = str_replace('۱', '1', $outp);
            $outp = str_replace('۲', '2', $outp);
            $outp = str_replace('۳', '3', $outp);
            $outp = str_replace('۴', '4', $outp);
            $outp = str_replace('۵', '5', $outp);
            $outp = str_replace('۶', '6', $outp);
            $outp = str_replace('۷', '7', $outp);
            $outp = str_replace('۸', '8', $outp);
            $outp = str_replace('۹', '9', $outp);
            return($outp);
        }

        $out = array();
        $conf = new conf();
        $url = $conf->wurl . "rest/lowfaresearch";
        $User = $conf->wuser;
        $Password = $conf->wpass;
        $dtmp = explode('-', perToEnNums($date));
        $mtmp = jalali_to_jgregorian($dtmp[0], $dtmp[1], $dtmp[2]);
        $date = $mtmp[0] . '-' . str_pad($mtmp[1], 2, "0", STR_PAD_LEFT) . '-' . str_pad($mtmp[2], 2, "0", STR_PAD_LEFT);
        $timestamp = date('Y-m-dTH:i:s+3:30');
        $data = '<OTA_AirLowFareSearchRQ xmlns="http://www.opentravel.org/OTA/2003/05"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 
                    OTA_AirLowFareSearchRQ.xsd" EchoToken="1234" TimeStamp="' . $timestamp . '"
                    Target="Real" Version="2.001" SequenceNmbr="1" PrimaryLangID="En-us">	
                          <OriginDestinationInformation>
                                  <DepartureDateTime>' . $date . '</DepartureDateTime>
                                  <OriginLocation LocationCode="' . $from_city . '"/>
                                  <DestinationLocation LocationCode="' . $to_city . '"/>
                          </OriginDestinationInformation>';
        if ($rdate != '') {
            $dtmp = explode('-', perToEnNums($rdate));
//            var_dump($dtmp);
            $mtmp = jalali_to_jgregorian($dtmp[0], $dtmp[1], $dtmp[2]);
            $rdate = $mtmp[0] . '-' . str_pad($mtmp[1], 2, "0", STR_PAD_LEFT) . '-' . str_pad($mtmp[2], 2, "0", STR_PAD_LEFT);
            $data .= '<OriginDestinationInformation>
                            <DepartureDateTime>' . $rdate . '</DepartureDateTime>
                            <OriginLocation LocationCode="' . $to_city . '"/>
                            <DestinationLocation LocationCode="' . $from_city . '"/>
                    </OriginDestinationInformation>';
        }
        $data .= '</OTA_AirLowFareSearchRQ>';
//        echo "REQUEST : <br/>\n" . $data;
        $json = search_class::sendRequest($url, $User, $Password, $data);
//        echo "<br/>\nRESPONSE : <br/>\n" . $json;
        $new_out = json_decode($json, TRUE);
//        var_dump($new_out);
        $out = $new_out; //[0];
        return($out);
    }

}
