<?php

require_once ('lib/nusoap.php');

class reserve_class {

    public $client;

    public function __construct() {
        
    }

    public static function sendRequest($url, $user, $pass, $data) {
        $options = array(
            'http' => array(
                'header' => "User: $user\r\nPassword: $pass\r\nContent-Type: application/xml\r\n",
                'method' => 'POST',
                'content' => $data
            ),
        );
        $context = stream_context_create($options);
        $out = file_get_contents($url, false, $context);
        return($out);
    }

    public static function status($refrence_id) {
        $conf = new conf();
        $out['err']['code'] = 8;
        $out['err']['msg'] = 'UNKNOWN ERROR';
        $client = new nusoap_client($conf->wurl . "server.php?wsdl", true);
        $arguments = array(
            "user" => $conf->wuser,
            "pass" => $conf->wpass,
            "refrence_id" => $refrence_id
        );
        $result = $client->call("reserve_status", $arguments);
        if ($client->fault) {
            $out['err']['code'] = 8;
            $out['err']['msg'] = $client->fault;
            echo '<hr/>';
            echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
            echo '<hr/>';
            echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
            echo '<hr/>';
            echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
        } else {
            $error = $client->getError();
            if ($error) {
                $out['err']['code'] = 8;
                $out['err']['msg'] = $error;
                echo '<hr/>';
                echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
                echo '<hr/>';
                echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
                echo '<hr/>';
                echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
            } else {
                $out = json_decode($result, TRUE);
            }
        }
        return($out);
    }

    public static function tickets($refrence_id) {
        $conf = new conf();
        $out['err']['code'] = 8;
        $out['err']['msg'] = 'UNKNOWN ERROR';
        $client = new nusoap_client($conf->wurl . "server.php?wsdl", true);
        $arguments = array(
            "user" => $conf->wuser,
            "pass" => $conf->wpass,
            "refrence_id" => $refrence_id
        );
        $result = $client->call("reserve_tickets", $arguments);
        if ($client->fault) {
            $out['err']['code'] = 8;
            $out['err']['msg'] = $client->fault;
            echo '<hr/>';
            echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
            echo '<hr/>';
            echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
            echo '<hr/>';
            echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
        } else {
            $error = $client->getError();
            if ($error) {
                $out['err']['code'] = 8;
                $out['err']['msg'] = $error;
                echo '<hr/>';
                echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
                echo '<hr/>';
                echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
                echo '<hr/>';
                echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
            } else {
                $out = json_decode($result, TRUE);
            }
        }
        return($out);
    }


    public static function confirm($refrence_id) {
        $conf = new conf();
        $out['err']['code'] = 8;
        $out['err']['msg'] = 'UNKNOWN ERROR';
//        $client = new nusoap_client($conf->wurl . "server.php?wsdl", true);
//        $arguments = array(
//            "user" => $conf->wuser,
//            "pass" => $conf->wpass,
//            "refrence_id" => $refrence_id
//        );
//        $result = $client->call("reserve_confirm2", $arguments);
//        if ($client->fault) {
//            $out['err']['code'] = 8;
//            $out['err']['msg'] = $client->fault;
//            echo '<hr/>';
//            echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
//            echo '<hr/>';
//            echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
//            echo '<hr/>';
//            echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
//        } else {
//            $error = $client->getError();
//            if ($error) {
//                $out['err']['code'] = 8;
//                $out['err']['msg'] = $error;
//                echo '<hr/>';
//                echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
//                echo '<hr/>';
//                echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
//                echo '<hr/>';
//                echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
//            } else {
//                $out = json_decode($result, TRUE);
//            }
//        }
        $url = "http://185.55.225.69/rest/confirm";
        $User = $conf->wuser;
        $Password = $conf->wpass;
        $req = '<OTA_AirBookConfirmRQ xmlns="http://www.opentravel.org/OTA/2003/05" 
                      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                      xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05  
                       OTA_AirBookRQ.xsd" EchoToken="50987" TimeStamp="2015-11-02IRST16:06:12+03:30" 
                      Target="Real" Version="2.001" SequenceNmbr="1" PrimaryLangID="En-us"> 
                     <AirReservation CreatedDateTme="4564656"> 
                     <BookingReferenceID ID="'.$refrence_id.'" ID_Context="BookingRef"/> 
                     </AirReservation> 
                     </OTA_AirBookConfirmRQ>';
//        echo "REQUEST:<br/>\n";
//        echo $req;
        $content = reserve_class::sendRequest($url, $User, $Password, $req);
//        echo "<br/>\nRESPONSE:<br/>\n";
//        echo $content;
        $xml = new SimpleXMLElement($content);
        if (empty($xml->Errors)) {
            foreach ($xml->AirReservation->Ticketing as $ticket) {
                $tickets[] = (string)$ticket->attributes()->TicketDocumentNbr;
                $out['tickets'] = $tickets;
            }
//            $return = [
//                'result' => 'success',
//                'tickets' => $tickets,
//                'errors' => []
//            ];
            $out['err']['code'] = 0;
            $out['err']['msg'] = '';
        } else {
            $errors = (array) $xml->Errors;
            $error = (array) $errors['Error'];
            $code = $error["@attributes"]['Code'];
            $msg = $error["@attributes"]['Message'];
            $out['err']['code'] = $code;
            $out['err']['msg'] = $msg;
            $out['refrence_id'] = '';
//            $return = [
//                'result' => 'failed',
//                'tickets' => [],
//                'errors' => $errors
//            ];
        }
        return($out);
    }    
    
    public static function confirm۲($refrence_id) {
        $conf = new conf();
        $out['err']['code'] = 8;
        $out['err']['msg'] = 'UNKNOWN ERROR';
        $client = new nusoap_client($conf->wurl . "server.php?wsdl", true);
        $arguments = array(
            "user" => $conf->wuser,
            "pass" => $conf->wpass,
            "refrence_id" => $refrence_id
        );
        $result = $client->call("reserve_confirm2", $arguments);
        if ($client->fault) {
            $out['err']['code'] = 8;
            $out['err']['msg'] = $client->fault;
            echo '<hr/>';
            echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
            echo '<hr/>';
            echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
            echo '<hr/>';
            echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
        } else {
            $error = $client->getError();
            if ($error) {
                $out['err']['code'] = 8;
                $out['err']['msg'] = $error;
                echo '<hr/>';
                echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
                echo '<hr/>';
                echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
                echo '<hr/>';
                echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
            } else {
                $out = json_decode($result, TRUE);
            }
        }
        return($out);
    }

    public static function preReserve($source_id, $flight_id, $ncap, $class_ghimat, $adl, $chd, $inf, $ip, $agency_id) {
//        $conf = new conf();
//        $out['err']['code'] = 8;
//        $out['err']['msg'] = 'UNKNOWN ERROR';
//        $client = new nusoap_client($conf->wurl."server.php?wsdl", true);
//        $arguments = array(
//            "user" => $conf->wuser,
//            "pass" => $conf->wpass,
//            "source_id" => $source_id,
//            "flight_id" => $flight_id,
//            "ncap" => $ncap,
//            "class_ghimat" => $class_ghimat,
//            "adl" => $adl,
//            "chd" => $chd,
//            "inf" => $inf,
//            "ip" => $ip,
//            "agency_id" => $agency_id
//        );
////        echo "ticketyab -> SE<br/>";
////        var_dump($arguments);
//        $result = $client->call("reserve_start2", $arguments);
//        if ($client->fault) {
//            $out['err']['code'] = 8;
//            $out['err']['msg'] = $client->fault;
//        } else {
//            $error = $client->getError();
//            if ($error) {
//                $out['err']['code'] = 8;
//                $out['err']['msg'] = $error;
//                echo '<hr/>';
//                echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
//                echo '<hr/>';
//                echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
//                echo '<hr/>';
//                echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
//            } else {
//                $out = json_decode($result, TRUE);
//            }
//        }
        $out['err']['code'] = 0;
        return($out);
    }

    public static function passengers($refID, $passengers, $mobile, $email, $address, $description, $flight1, $flight2) {
        $out['err']['code'] = 8;
        $out['err']['msg'] = 'UNKNOWN ERROR';
        $conf = new conf();
        $url = $conf->wurl . "rest/book";
        $User = $conf->wuser;
        $Password = $conf->wpass;
        $npass = array();
        foreach ($passengers as $i => $p) {
            $birthday = explode('-', $p['birthday']);
            $bdate_arr = jalali_to_jgregorian($birthday[0], $birthday[1], $birthday[2]);

            $passenger = array(
                "gender" => ($p['gender'] == 1) ? 'M' : 'F',
                "fname" => $p['fname_en'],
                "lname" => $p['lname_en'],
                'nationality' => 'IRN',
                'nationalCode' => (trim($p['passport_engheza']) != '') ? '' : $p['shomare_melli'],
                "passportId" => (trim($p['passport_engheza']) == '') ? '' : $p['shomare_melli'],
                "passportExpire" => trim($p['passport_engheza']),
                'passportIssueCountry' => 'IRN',
                "birthDate" => $bdate_arr[0] . '-' . $bdate_arr[1] . '-' . $bdate_arr[2]
            );
            $npass[] = $passenger;
        }
        $dtmp = explode('-', $flight1['fdate']);
        $mtmp = jalali_to_jgregorian($dtmp[0], $dtmp[1], $dtmp[2]);
        $flight1['fdate'] = $mtmp[0] . '-' . $mtmp[1] . '-' . $mtmp[2];
        $flight1['arrivalDate'] = $flight1['fdate'];
        if ($flight1['ltime'] < $flight1['ftime']) {
            $flight1['arrivalDate'] = date("Y-m-d", strtotime($flight1['fdate'] . ' + 1 day'));
        }

        $req = ' <OTA_AirBookRQ xmlns="http://www.opentravel.org/OTA/2003/05" 
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                   xsi:schemaLocation="http://www.opentravel.org/OTA/2008/05  
                   OTA_AirBookRQ.xsd" EchoToken="50987" TimeStamp="2015-11-02IRST16:06:12+03:30" 
                   Target="Real" Version="2.001" SequenceNmbr="1" PrimaryLangID="En-us"> 
        <AirItinerary DirectionInd="OneWay"> 
            <OriginDestinationOptions> 
                <OriginDestinationOption> 
                    <FlightSegment TFlight="' . $flight1['tflight'] . '" FlightNumber="' . $flight1['flight_number'] . '" ResBookDesigCode="H" DepartureDateTime="' . $flight1['fdate'] . 'T' . $flight1['ftime'] . ':00.000+03:30" 
                                   ArrivalDateTime="' . $flight1['arrivalDate'] . 'T' . $flight1['ltime'] . ':00.000+03:30" StopQuantity="0" RPH="1"> 
                        <DepartureAirport LocationCode="' . $flight1['from_city'] . '" Terminal="T1"/> 
                        <ArrivalAirport LocationCode="' . $flight1['to_city'] . '" Terminal="T2"/> 
                    </FlightSegment> 
                </OriginDestinationOption> 
            </OriginDestinationOptions> 
        </AirItinerary> 
        <PriceInfo> 
            <ItinTotalFare> 
                <TotalFare CurrencyCode="IRR" DecimalPlaces="0" Amount="' . $flight1['price'] . '"/> 
            </ItinTotalFare> 
        </PriceInfo> 
        <TravelerInfo>';
        foreach ($npass as $passenger) {
            $req.='<AirTraveler PassengerTypeCode="ADT" Gender="' . $passenger['gender'] . '" BirthDate="' . $passenger['birthDate'] . '" AccompaniedByInfantInd="false"> 
                <PersonName> 
                    <NamePrefix>Mr</NamePrefix> 
                    <GivenName>' . $passenger['fname'] . '</GivenName>  
                    <Surname>' . $passenger['lname'] . '</Surname> 
                </PersonName> 
                <Telephone PhoneNumber="' . $mobile . '"/> 
                <Email>' . $email . '</Email> 
                <Document DocID="' . $passenger['passportId'] . '" DocHolderNationality="' . $passenger['passportIssueCountry'] . '"/>  
            </AirTraveler>';
        }
        $req.='</TravelerInfo> 
        </OTA_AirBookRQ>';

//        echo "REQUEST : <br/>\n" . $req;
        $content = reserve_class::sendRequest($url, $User, $Password, $req);
//        echo "<br/>\nRESPONSE:<br/>\n" . $content;
        $xml = new SimpleXMLElement($content);

        if (empty($xml->Errors)) {
            $t = (array) $xml->AirReservation->BookingReferenceID;
            $refrenceId = $t['@attributes']['ID'];
//            $return = [
//                'result' => 'success',
//                'refrenceId' => $refrenceId,
//                'errors' => []
//            ];
            $out['err']['code'] = 0;
            $out['err']['msg'] = '';
            $out['refrence_id'] = $refrenceId;
        } else {
//            var_dump((array) $xml->Errors);
//            foreach ((array) $xml->Errors as $index => $node) {
//                $n = (array)$node[0];
//                $node= (array)$n[0];
//                var_dump($node);
//                $code = $node["@attributes"]['Code'];
//                $msg = $node["@attributes"]['Message'];
//                $out['err']['code'] = $code;
//                $out['err']['msg'] = $msg;
//            }
            $errors = (array) $xml->Errors;
            $error = (array) $errors['Error'];
            $code = $error["@attributes"]['Code'];
            $msg = $error["@attributes"]['Message'];
            $out['err']['code'] = $code;
            $out['err']['msg'] = $msg;
            $out['refrence_id'] = '';
        }

//        } else {
//            foreach ((array) $xml->Errors as $index => $node) {
//                foreach ($node as $key => $n) {
//                    $code = (string) $n->attributes()->Code . '<br/>';
//                    $msg = (string) $n->attributes()->Message;
//                    $out['err']['code'] = $code;
//                    $out['err']['msg'] = $msg;
//                }
//            }
//            $out['refrence_id'] = '';
//        }
//        $client = new nusoap_client($conf->wurl."server.php?wsdl", true);
//        $arguments = array(
//            "user" => $conf->wuser,
//            "pass" => $conf->wpass,
//            "refrence_id" => $refID,
//            "jpassengers" => json_encode($passengers),
//            "mobile" => $mobile,
//            "email" => $email,
//            "address" => $address,
//            "description" => $description
//        );
//        $result = $client->call("reserve_passengers2", $arguments);
//        if ($client->fault) {
//            $out['err']['code'] = 8;
//            $out['err']['msg'] = $client->fault;
//        } else {
//            $error = $client->getError();
//            if ($error) {
//                $out['err']['code'] = 8;
//                $out['err']['msg'] = $error;
//                echo '<hr/>';
//                echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
//                echo '<hr/>';
//                echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
//                echo '<hr/>';
//                echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
//            } else {
//                $out = json_decode($result, TRUE);
//            }
//        }
        return($out);
    }

}
